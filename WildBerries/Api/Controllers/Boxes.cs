using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    // [Route("boxes/")]
    public class BoxesController : Controller
    { 
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public BoxesController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("boxes/box/{id}")]
        public virtual IActionResult DeleteBoxId([FromRoute][Required]int id)
        {
            var box = _dbContext.Boxes.FirstOrDefault(b => b.Id == id);

            if (box == null)
            {
                return BadRequest();
            }

            _dbContext.Boxes.Remove(box);
            
            return StatusCode(200);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("boxes/all")]
        public virtual IActionResult Get()
        { 
            var boxes = _dbContext.Boxes.ToList();
            
            return new ObjectResult(boxes);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("boxes/box/{id}")]
        public virtual IActionResult GetBox([FromRoute][Required]int id)
        { 
            var box = _dbContext.Boxes.FirstOrDefault(b => b.Id == id);

            if (box == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(box);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("boxes/box")]
        public virtual IActionResult PostBox()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }

            var box = JsonSerializer.Deserialize<Box>(bodyString);
            var res = TryValidateModel(box);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }
            
            _dbContext.Boxes.Add(box);
            _dbContext.SaveChanges();
            
            return new ObjectResult(box);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("boxes/box")]
        public virtual IActionResult PutBoxId([FromQuery][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}
