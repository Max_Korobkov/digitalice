using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    // [Route("buyers/[action]")]
    public class BuyersController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public BuyersController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("buyers/buyer/{id}")]
        public virtual IActionResult DeleteBuyer([FromRoute][Required]int id)
        { 
            var buyer = _dbContext.Buyers.FirstOrDefault(b => b.Id == id);

            if (buyer == null)
            {
                return BadRequest();
            }

            _dbContext.Buyers.Remove(buyer);
            
            return StatusCode(200);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("buyers/buyer/{id}")]
        public virtual IActionResult GetBuyer([FromRoute][Required]int id)
        { 
            var buyer = _dbContext.Buyers.FirstOrDefault(b => b.Id == id);

            if (buyer == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(buyer);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("buyers/all")]
        public virtual IActionResult Get()
        { 
            var buyers = _dbContext.Buyers.ToList();

            return new ObjectResult(buyers);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("buyers/buyer")]
        public virtual IActionResult PostBuyers()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }

            var buyer = JsonSerializer.Deserialize<Buyer>(bodyString);
            var res = TryValidateModel(buyer);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }
            
            _dbContext.Buyers.Add(buyer);
            _dbContext.SaveChanges();
            
            return new ObjectResult(buyer);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("buyers/buyer/{id}")]
        public virtual IActionResult PutBuyer([FromRoute][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}
