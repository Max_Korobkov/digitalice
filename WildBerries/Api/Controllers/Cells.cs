using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    // [Route("cells/[action]")]
    public class CellsController : Controller
    {
        
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public CellsController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("cells/cell/{id}")]
        public virtual IActionResult DeleteCell([FromRoute][Required]int id)
        { 
            var cell = _dbContext.Cells.FirstOrDefault(b => b.Id == id);

            if (cell == null)
            {
                return BadRequest();
            }

            _dbContext.Cells.Remove(cell);
            
            return StatusCode(200);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("cells/cell/{id}")]
        public virtual IActionResult GetCell([FromRoute][Required]int id)
        { 
            var cell = _dbContext.Cells.FirstOrDefault(b => b.Id == id);

            if (cell == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(cell);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("cells/all")]
        public virtual IActionResult Get()
        { 
            var cells = _dbContext.Cells.ToList();
            
            return new ObjectResult(cells);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("cells/cell")]
        public virtual IActionResult PostCell()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }

            var cell = JsonSerializer.Deserialize<Cell>(bodyString);
            var res = TryValidateModel(cell);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }
            
            _dbContext.Cells.Add(cell);
            _dbContext.SaveChanges();
            
            return new ObjectResult(cell);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("cells/cell/{id}")]
        public virtual IActionResult PutCell([FromRoute][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}
