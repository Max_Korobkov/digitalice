using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    // [Route("global-things/[action]")]
    public class GlobalThingsController : Controller
    { 
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public GlobalThingsController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("global-things/global-thing/{id}")]
        public virtual IActionResult DeleteShopGlobalThing([FromRoute][Required]int id)
        { 
            var globalThing = _dbContext.GlobalThings.FirstOrDefault(b => b.Id == id);

            if (globalThing == null)
            {
                return BadRequest();
            }

            _dbContext.GlobalThings.Remove(globalThing);
            
            return StatusCode(200);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("global-things/all")]
        public virtual IActionResult Get()
        { 
            var globalThings = _dbContext.GlobalThings.ToList();
            
            return new ObjectResult(globalThings);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("global-things/global-thing/{id}")]
        public virtual IActionResult GetShopGlobalThing([FromRoute][Required]int id)
        { 
            var globalThing = _dbContext.GlobalThings.FirstOrDefault(b => b.Id == id);

            if (globalThing == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(globalThing);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("global-things/global-thing")]
        public virtual IActionResult PostGlobalThing()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }
            
            var globalThing = JsonSerializer.Deserialize<GlobalThing>(bodyString);
            var res = TryValidateModel(globalThing);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }
            
            _dbContext.GlobalThings.Add(globalThing);
            _dbContext.SaveChanges();
            
            return new ObjectResult(globalThing);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("global-things/global-thing/{id}")]
        public virtual IActionResult PutShopGlobalThing([FromRoute][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}
