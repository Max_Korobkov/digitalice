using System.Linq;
using Database.Models;
using Microsoft.AspNetCore.Mvc;
using Main = Api.Models.Main;

namespace Api.Controllers
{
    [Route("system/[action]")]
    public class MainController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;
        
        private Main _mainDb = new Main();
        private bool _isInternetAvailible = true;

        public MainController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        [HttpPatch]
        [ActionName("sync")]
        public virtual IActionResult SetDataInStorage()
        {
            if (_isInternetAvailible)
            {
                foreach (var retBox in _dbContext.ReturnBoxes.Where(p => p.IsChanged))
                {
                    var match = _mainDb.ReturnBoxes.FirstOrDefault(b => b.Id == retBox.Id);
                    if (match != null)
                    {
                        match.Id = retBox.Id;
                        match.Status = retBox.Status;
                    }
                    else
                    {
                        _mainDb.ReturnBoxes.Add(retBox);
                    }
                }
                
                foreach (var orderedThing in _dbContext.OrderedThings.Where(p => p.IsChanged))
                {
                    var match = _mainDb.OrderedThings.FirstOrDefault(b => b.Id == orderedThing.Id);
                    if (match != null)
                    {
                        match.Id = orderedThing.Id;
                        match.BoxId = orderedThing.BoxId;
                        match.IsConfirmed = true;
                        match.OrderId = orderedThing.OrderId;
                        match.GlobalThingId = orderedThing.GlobalThingId;
                        match.OrderedThingStatus = orderedThing.OrderedThingStatus == OrderedThingStatus.Returned ? 
                            OrderedThingStatus.Returned : 
                            OrderedThingStatus.Accepted;
                        match.OrderedThingPrice = orderedThing.OrderedThingPrice;
                    }
                    else
                    {
                        _mainDb.WrongThings.Add(new WrongThing(orderedThing.Id, ErrorStatus.More));
                    }
                }
                
                foreach (var ret in _dbContext.Returns.Where(p => p.IsChanged))
                {
                    var match = _mainDb.Returns.FirstOrDefault(b => b.OrderedThingId == ret.OrderedThingId);
                    if (match != null)
                    {
                        match.OrderedThingId = ret.OrderedThingId;
                        match.ReturnBoxId = ret.ReturnBoxId;
                        match.ReturnType = ret.ReturnType;
                        match.IsReturnApproved = ret.IsReturnApproved;
                    }
                    else
                    {
                        _mainDb.Returns.Add(ret);
                    }
                }
                
                foreach (var wrongThing in _dbContext.WrongThings.Where(p => p.IsChanged))
                {
                    var match = _mainDb.WrongThings.FirstOrDefault(b => b.Id == wrongThing.Id);
                    if (match != null)
                    {
                        match.Id = wrongThing.Id;
                        match.Status = wrongThing.Status;
                    }
                    else
                    {
                        _mainDb.WrongThings.Add(wrongThing);
                    }
                }
                
                GetFromStorage();
                
                return StatusCode(200);
            }
            
            return StatusCode(503);
        }
        
        [HttpPatch]
        [ActionName("load")]
        public IActionResult GetFromStorage()
        {
            if (_isInternetAvailible)
            {
                _dbContext.Boxes.RemoveRange(_dbContext.Boxes);
                _dbContext.Buyers.RemoveRange(_dbContext.Buyers);
                _dbContext.GlobalThings.RemoveRange(_dbContext.GlobalThings);
                _dbContext.ReturnBoxes.RemoveRange(_dbContext.ReturnBoxes);
                _dbContext.Orders.RemoveRange(_dbContext.Orders);
                _dbContext.Cells.RemoveRange(_dbContext.Cells);
                _dbContext.OrderedThings.RemoveRange(_dbContext.OrderedThings);
                _dbContext.Returns.RemoveRange(_dbContext.Returns);
                _dbContext.WrongThings.RemoveRange(_dbContext.WrongThings);
                _dbContext.SaveChanges();
            
                _dbContext.Boxes.AddRange(_mainDb.Boxes);
                _dbContext.Buyers.AddRange(_mainDb.Buyers);
                _dbContext.GlobalThings.AddRange(_mainDb.GlobalThings);
                _dbContext.ReturnBoxes.AddRange(_mainDb.ReturnBoxes);
                _dbContext.Orders.AddRange(_mainDb.Orders);
                _dbContext.Cells.AddRange(_mainDb.Cells);
                _dbContext.OrderedThings.AddRange(_mainDb.OrderedThings);
                _dbContext.Returns.AddRange(_mainDb.Returns);
                _dbContext.WrongThings.AddRange(_mainDb.WrongThings);
                _dbContext.SaveChanges();
                
                _mainDb.Boxes.Add(new Box(77, 0));
                _mainDb.Buyers.Add(new Buyer(77, "Alex", "Ban", "Ban", "89161270101"));
                _mainDb.GlobalThings.Add(new GlobalThing(87, "Pants", "Pants"));
                _mainDb.Orders.Add(new Order(77, 77, true));
                _mainDb.Cells.Add(new Cell(77, 77, true));
                _mainDb.OrderedThings.Add(new OrderedThing(77, 87, 77, OrderedThingStatus.Send, 700, true));
                
                return StatusCode(200);
            }

            return StatusCode(503);
        }
        
        [HttpPatch]
        [ActionName("set-internet")]
        public virtual IActionResult SetInternet([FromQuery]bool isInternetSet)
        {
            _isInternetAvailible = isInternetSet;

            return StatusCode(200);
        }
        
        [HttpGet]
        [ActionName("get-internet")]
        public virtual IActionResult GetInternet()
        {
            return new ObjectResult(_isInternetAvailible);
        }
        
    }
}