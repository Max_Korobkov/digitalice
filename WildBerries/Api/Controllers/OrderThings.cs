using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    // [Route("ordered-things/[action]")]
    public class OrderedThingsController : Controller
    { 
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public OrderedThingsController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("ordered-things/ordered-thing/{id}")]
        public virtual IActionResult DeleteOrderThingId([FromRoute][Required]int id)
        { 
            var orderThing = _dbContext.OrderedThings.FirstOrDefault(b => b.Id == id);

            if (orderThing == null)
            {
                return BadRequest();
            }

            _dbContext.OrderedThings.Remove(orderThing);
            
            return StatusCode(200);
        }
        
        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("ordered-things/ordered-thing/{id}")]
        public virtual IActionResult GetOrderThing([FromRoute][Required]int id)
        { 
            var orderThing = _dbContext.OrderedThings.FirstOrDefault(b => b.Id == id);

            if (orderThing == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(orderThing);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("ordered-things/all")]
        public virtual IActionResult Get()
        { 
            var orderThings = _dbContext.OrderedThings.ToList();
            
            return new ObjectResult(orderThings);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("ordered-things/ordered-thing")]
        public virtual IActionResult PostOrderThing()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }
            
            var orderedThing = JsonSerializer.Deserialize<OrderedThing>(bodyString);
            var res = TryValidateModel(orderedThing);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }

            var thing = _dbContext.OrderedThings.FirstOrDefault(o => o.Id == orderedThing.Id);

            if (thing != null)
            {
                thing.OrderedThingStatus = OrderedThingStatus.Accepted;
                thing.IsChanged = true;
                _dbContext.SaveChanges();
                
                return new ObjectResult(thing);
            }

            orderedThing.IsConfirmed = false;
            orderedThing.IsChanged = true;
            _dbContext.OrderedThings.Add(orderedThing);
            
            _dbContext.SaveChanges();
            
            return new ObjectResult(orderedThing);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("ordered-things/ordered-thing/{id}")]
        public virtual IActionResult PutOrderThing([FromRoute][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}
