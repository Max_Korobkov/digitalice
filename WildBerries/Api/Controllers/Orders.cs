using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    // [Route("orders/[action]")]
    public class OrdersController : Controller
    { 
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public OrdersController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("orders/order/{id}")]
        public virtual IActionResult DeleteOrder([FromRoute][Required]int id)
        { 
            var order = _dbContext.Orders.FirstOrDefault(b => b.Id == id);

            if (order == null)
            {
                return BadRequest();
            }

            _dbContext.Orders.Remove(order);
            
            return StatusCode(200);
        }
        
        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("orders/order/{id}")]
        public virtual IActionResult GetOrder([FromRoute][Required]int id)
        { 
            var order = _dbContext.Orders.FirstOrDefault(b => b.Id == id);

            if (order == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(order);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("orders/all")]
        public virtual IActionResult Get()
        { 
            var orders = _dbContext.Orders.ToList();
            
            return new ObjectResult(orders);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("orders/order/")]
        public virtual IActionResult PostOrder()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }
            
            var order = JsonSerializer.Deserialize<Order>(bodyString);
            var res = TryValidateModel(order);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }
            
            _dbContext.Orders.Add(order);
            _dbContext.SaveChanges();
            
            return new ObjectResult(order);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("orders/order/{id}")]
        public virtual IActionResult PutOrder([FromRoute][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}
