using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    // [Route("return-boxes/[action]")]
    public class ReturnBoxesController : Controller
    { 
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public ReturnBoxesController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("return-boxes/return-box/{id}")]
        public virtual IActionResult DeleteReturnBox([FromRoute][Required]int id)
        { 
            var returnBox = _dbContext.ReturnBoxes.FirstOrDefault(b => b.Id == id);

            if (returnBox == null)
            {
                return BadRequest();
            }

            _dbContext.ReturnBoxes.Remove(returnBox);
            
            return StatusCode(200);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("return-boxes/return-box/{id}")]
        public virtual IActionResult GetReturnBox([FromRoute][Required]int id)
        { 
            var returnBox = _dbContext.ReturnBoxes.FirstOrDefault(b => b.Id == id);

            if (returnBox == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(returnBox);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("return-boxes/all")]
        public virtual IActionResult Get()
        { 
            var returnBoxes = _dbContext.ReturnBoxes.ToList();
            
            return new ObjectResult(returnBoxes);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("return-boxes/return-box")]
        public virtual IActionResult PostReturnBox()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }
            
            var returnBox = JsonSerializer.Deserialize<ReturnBox>(bodyString);
            var res = TryValidateModel(returnBox);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }
            
            _dbContext.ReturnBoxes.Add(returnBox);
            _dbContext.SaveChanges();
            
            return new ObjectResult(returnBox);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("return-boxes/return-box/{id}")]
        public virtual IActionResult PutReturnBox([FromRoute][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}
