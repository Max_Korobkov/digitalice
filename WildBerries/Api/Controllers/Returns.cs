using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    // [Route("returns/[action]")]
    public class ReturnsController : Controller
    { 
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public ReturnsController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("returns/return/{id}")]
        public virtual IActionResult DeleteReturn([FromRoute][Required]int id)
        { 
            var @return = _dbContext.Returns.FirstOrDefault(b => b.OrderedThingId == id);

            if (@return == null)
            {
                return BadRequest();
            }

            _dbContext.Returns.Remove(@return);
            
            return StatusCode(200);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("returns/return/{id}")]
        public virtual IActionResult GetReturn([FromRoute][Required]int id)
        { 
            var @return = _dbContext.Returns.FirstOrDefault(b => b.OrderedThingId == id);

            if (@return == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(@return);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("returns/all")]
        public virtual IActionResult Get()
        { 
            var returns = _dbContext.Returns.ToList();
            
            return new ObjectResult(returns);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("returns/return")]
        public virtual IActionResult PostReturn()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }
            
            var @return = JsonSerializer.Deserialize<Return>(bodyString);
            var res = TryValidateModel(@return);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }
            
            _dbContext.Returns.Add(@return);
            _dbContext.SaveChanges();
            
            return new ObjectResult(@return);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("returns/return/{id}")]
        public virtual IActionResult PutReturn([FromRoute][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}
