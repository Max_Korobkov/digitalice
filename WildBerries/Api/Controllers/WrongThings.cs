using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    // [Route("wrong-things/[action]")]
    public class WrongThingsController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly CalculationContext _dbContext;

        public WrongThingsController(CalculationContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpDelete]
        [Route("wrong-things/wrong-thing/{id}")]
        public virtual IActionResult DeleteWrongThingId([FromRoute][Required]int id)
        { 
            var wrongThing = _dbContext.WrongThings.FirstOrDefault(b => b.Id == id);

            if (wrongThing == null)
            {
                return BadRequest();
            }

            _dbContext.WrongThings.Remove(wrongThing);
            
            return StatusCode(200);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("wrong-things/all")]
        public virtual IActionResult Get()
        { 
            var wrongThings = _dbContext.WrongThings.ToList();
            
            return new ObjectResult(wrongThings);
        }

        /// <summary>
        /// Your GET endpoint
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("wrong-things/wrong-thing/{id}")]
        public virtual IActionResult GetWrongThingId([FromRoute][Required]int id)
        { 
            var wrongThing = _dbContext.WrongThings.FirstOrDefault(b => b.Id == id);

            if (wrongThing == null)
            {
                return BadRequest();
            }
            
            return new ObjectResult(wrongThing);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("wrong-things/wrong-thing")]
        public virtual IActionResult PostWrongThing()
        { 
            string bodyString;
            using (StreamReader reader = new StreamReader(Request.Body, Encoding.UTF8))
            {
                bodyString = reader.ReadToEnd();
            }
            
            var wrongThing = JsonSerializer.Deserialize<WrongThing>(bodyString);
            var res = TryValidateModel(wrongThing);
            if (!ModelState.IsValid)
            {
                return new BadRequestObjectResult(new ErrorDetails()
                {
                    Description = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage),
                    Message = "Bad request"
                });
            }
            
            _dbContext.WrongThings.Add(wrongThing);
            _dbContext.SaveChanges();
            
            return new ObjectResult(wrongThing);
        }

        /// <summary>
        /// 
        /// </summary>
        
        /// <param name="id"></param>
        /// <response code="200">OK</response>
        [HttpPut]
        [Route("wrong-things/wrong-thing/{id}")]
        public virtual IActionResult PutWrongThingId([FromRoute][Required]int id)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            return StatusCode(200);


            throw new NotImplementedException();
        }
    }
}