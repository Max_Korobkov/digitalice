﻿﻿using System.Collections;
 using System.Collections.Generic;
 using System.Text.Json;

 namespace Api.ErrorHandling
{
    public class ErrorDetails
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public IEnumerable<string> Description { get; set; }
        
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}