using System.Collections.Generic;
using Database.Models;

namespace Api.Models
{
    public class Main
    {
        public List<Order> Orders { get; set; }
        public List<Cell> Cells { get; set; }
        public List<Buyer> Buyers { get; set; }
        public List<OrderedThing> OrderedThings { get; set; }
        public List<GlobalThing> GlobalThings { get; set; }
        public List<Return> Returns { get; set; }
        public List<ReturnBox> ReturnBoxes { get; set; }
        public List<Box> Boxes { get; set; }
        public List<WrongThing> WrongThings  { get; set; }
        
        public Main()
        {
            Orders = new List<Order>() 
            {
                new Order(0, 0, true), 
                new Order(1, 1, true), 
                new Order(2, 2, true)
                
            };

            Cells = new List<Cell>()
            {
                new Cell(0, 0, false),
                new Cell(1, 1, false),
                new Cell(2, 2, false)
            };
            
            Buyers = new List<Buyer>()
            {
                new Buyer(0, "Kirill", "Pozdnyakov","Evgenevich", "88005553535"),
                new Buyer(1, "Nikolay", "Archangelskiy","Sergeevich", "890006663636"),
                new Buyer(2, "Maxim", "Korobkov","Andreevich", "89995052233")
            };

            // true or false
            OrderedThings = new List<OrderedThing>()
            {
                new OrderedThing(0, 0, 0, OrderedThingStatus.Formed, 500,
                    true),
                new OrderedThing(1, 1, 1, OrderedThingStatus.Send, 100,
                    true),
            };

            GlobalThings = new List<GlobalThing>()
            {
                new GlobalThing(0, "Milk", "Milk descr"),
                new GlobalThing(1, "T-shirt", "T-shirt descr"),
                new GlobalThing(2, "Jacket", "Jacket descr")
            };

            Returns = new List<Return>()
            {
                new Return(0, 0, ReturnType.Reclamation, false),
                new Return(1, 1, ReturnType.Reclamation, false),
            };
            
            ReturnBoxes = new List<ReturnBox>()
            {
                new ReturnBox(0, ReturnBoxStatus.Sended),
                new ReturnBox(1, ReturnBoxStatus.Sended),
                new ReturnBox(2, ReturnBoxStatus.Sended)
            };
            
            Boxes = new List<Box>()
            {
                new Box(0, 0),
                new Box(1, 1)
            };
            
            WrongThings = new List<WrongThing>()
            {
                new WrongThing(0, ErrorStatus.Less),
                new WrongThing(1, ErrorStatus.Less),
                new WrongThing(2, ErrorStatus.Less)
            };
            
        }
    }
}