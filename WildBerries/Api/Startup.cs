using System;
using Api.ErrorHandling;
using Database.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Api
{
    public class Startup
    {
        private IConfiguration _configuration { get; }
        private ILogger<Startup> _logger { get; }

        public Startup(IWebHostEnvironment env, ILogger<Startup> logger)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            _configuration = builder.Build();
            _logger = logger;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CalculationContext>(CreateCalculationContext(_configuration));
            services.AddMvc(options => options.EnableEndpointRouting = false);
            services.AddCors();
        }

        private Action<DbContextOptionsBuilder> CreateCalculationContext(IConfiguration configuration)
        {
            return (optionsBuilder) =>
            {
                optionsBuilder
                    .UseNpgsql(
                        configuration.GetConnectionString("Connection"),
                        options => options.EnableRetryOnFailure(3));
            };
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.ConfigureExceptionHandler(_logger);
            app.UseCors(
                o => o
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
            );
            app.UseMvc();
        }
    }
}