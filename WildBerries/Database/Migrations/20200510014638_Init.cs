﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Database.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Boxes",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    destination_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Boxes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Buyers",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    first_name = table.Column<string>(type: "text", nullable: true),
                    second_name = table.Column<string>(type: "text", nullable: true),
                    patronymic = table.Column<string>(type: "text", nullable: true),
                    phone = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buyers", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "GlobalThings",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    name = table.Column<string>(type: "text", nullable: true),
                    description = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GlobalThings", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ReturnBoxes",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    status = table.Column<int>(type: "integer", nullable: false),
                    is_changed = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReturnBoxes", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    buyer_id = table.Column<int>(type: "integer", nullable: false),
                    is_complected = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.id);
                    table.ForeignKey(
                        name: "FK_Orders_Buyers_buyer_id",
                        column: x => x.buyer_id,
                        principalTable: "Buyers",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cells",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    order_id = table.Column<int>(type: "integer", nullable: false),
                    is_full = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cells", x => x.id);
                    table.ForeignKey(
                        name: "FK_Cells_Orders_order_id",
                        column: x => x.order_id,
                        principalTable: "Orders",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderedThings",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    global_thing_id = table.Column<int>(type: "integer", nullable: false),
                    order_id = table.Column<int>(type: "integer", nullable: false),
                    box_id = table.Column<int>(type: "integer", nullable: false),
                    ordered_thing_status = table.Column<int>(type: "integer", nullable: false),
                    ordered_thing_price = table.Column<double>(type: "double precision", nullable: false),
                    is_confirmed = table.Column<bool>(type: "boolean", nullable: false),
                    is_changed = table.Column<bool>(type: "boolean", nullable: false),
                    is_get_before_sync = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderedThings", x => x.id);
                    table.ForeignKey(
                        name: "FK_OrderedThings_Boxes_id",
                        column: x => x.id,
                        principalTable: "Boxes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderedThings_GlobalThings_id",
                        column: x => x.id,
                        principalTable: "GlobalThings",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderedThings_Orders_id",
                        column: x => x.id,
                        principalTable: "Orders",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Returns",
                columns: table => new
                {
                    ordered_thing_id = table.Column<int>(type: "integer", nullable: false),
                    return_box_id = table.Column<int>(type: "integer", nullable: false),
                    return_type = table.Column<int>(type: "integer", nullable: false),
                    is_return_approved = table.Column<bool>(type: "boolean", nullable: false),
                    is_changed = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Returns", x => x.ordered_thing_id);
                    table.ForeignKey(
                        name: "FK_Returns_OrderedThings_ordered_thing_id",
                        column: x => x.ordered_thing_id,
                        principalTable: "OrderedThings",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Returns_ReturnBoxes_return_box_id",
                        column: x => x.return_box_id,
                        principalTable: "ReturnBoxes",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WrongThings",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false),
                    error_status = table.Column<int>(type: "integer", nullable: false),
                    is_changed = table.Column<bool>(type: "boolean", nullable: false),
                    OrderedThingId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WrongThings", x => x.id);
                    table.ForeignKey(
                        name: "FK_WrongThings_OrderedThings_OrderedThingId",
                        column: x => x.OrderedThingId,
                        principalTable: "OrderedThings",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cells_order_id",
                table: "Cells",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_buyer_id",
                table: "Orders",
                column: "buyer_id");

            migrationBuilder.CreateIndex(
                name: "IX_Returns_return_box_id",
                table: "Returns",
                column: "return_box_id");

            migrationBuilder.CreateIndex(
                name: "IX_WrongThings_OrderedThingId",
                table: "WrongThings",
                column: "OrderedThingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cells");

            migrationBuilder.DropTable(
                name: "Returns");

            migrationBuilder.DropTable(
                name: "WrongThings");

            migrationBuilder.DropTable(
                name: "ReturnBoxes");

            migrationBuilder.DropTable(
                name: "OrderedThings");

            migrationBuilder.DropTable(
                name: "Boxes");

            migrationBuilder.DropTable(
                name: "GlobalThings");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Buyers");
        }
    }
}
