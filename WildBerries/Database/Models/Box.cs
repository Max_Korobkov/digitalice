using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class Box
    {
        [Key]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("destination_id")]
        [JsonPropertyName("destination_id")]
        public int DestinationId { get; set; }
        
        [JsonIgnore]
        public List<OrderedThing> OrderedThings { get; set; }
        
        public Box(int id, int destinationId)
        {
            Id = id;
            DestinationId = destinationId;
        }
        
        public Box(){}
        
        public Box Clone()
        {
            return new Box()
            {
                Id = Id,
                DestinationId = DestinationId,
            };
        }
    }
}