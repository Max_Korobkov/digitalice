using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class Buyer
    {
        [Key]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("first_name")]
        [JsonPropertyName("first_name")]
        public string FirstName { get; set; }
        
        [Column("second_name")]
        [JsonPropertyName("second_name")]
        public string SecondName { get; set; }
        
        [Column("patronymic")]
        [JsonPropertyName("patronymic")]
        public string Patronymic { get; set; }
        
        [Column("phone")]
        [JsonPropertyName("phone")]
        public string Phone { get; set; }
        
        [JsonIgnore]
        public List<Order> Orders { get; set; }
        
        public Buyer(int id, string firstName, string seecondName, string patronymic, string phone)
        {
            Id = id;
            FirstName = firstName;
            SecondName = seecondName;
            Patronymic = patronymic;
            Phone = phone;
        }
        
        public Buyer(){}
        
        public Buyer Clone()
        {
            return new Buyer()
            {
                Id = Id,
                FirstName = FirstName,
                Patronymic = Patronymic,
                Phone = Phone,
            };
        }
    }
}