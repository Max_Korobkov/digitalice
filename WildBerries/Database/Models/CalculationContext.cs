﻿using Microsoft.EntityFrameworkCore;

namespace Database.Models
{
    public class CalculationContext : DbContext
    {
        public CalculationContext(DbContextOptions optionsBuilder) : base(optionsBuilder)
        { }
        
        public DbSet<Order> Orders { get; set; }
        public DbSet<Cell> Cells { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<OrderedThing> OrderedThings { get; set; }
        public DbSet<GlobalThing> GlobalThings { get; set; }
        public DbSet<Return> Returns { get; set; }
        public DbSet<ReturnBox> ReturnBoxes { get; set; }
        public DbSet<Box> Boxes { get; set; }
        public DbSet<WrongThing> WrongThings  { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cell>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<Order>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<Buyer>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<OrderedThing>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<WrongThing>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<GlobalThing>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<Return>()
                .Property(p => p.OrderedThingId)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<ReturnBox>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<Box>()
                .Property(p => p.Id)
                .ValueGeneratedNever();
            
            modelBuilder.Entity<Cell>()
                .HasOne(p => p.Order)
                .WithMany(p => p.Cells)
                .HasForeignKey(p => p.OrderId);

            modelBuilder.Entity<Order>()
                .HasOne(p => p.Buyer)
                .WithMany(p => p.Orders)
                .HasForeignKey(p => p.BuyerId);
            
            modelBuilder.Entity<OrderedThing>()
                .HasOne(p => p.Order)
                .WithMany(p => p.OrderedThings)
                .HasForeignKey(p => p.Id);
            
            modelBuilder.Entity<OrderedThing>()
                .HasOne(p => p.GlobalThing)
                .WithMany(p => p.OrderedThings)
                .HasForeignKey(p => p.Id);
            
            modelBuilder.Entity<OrderedThing>()
                .HasOne(p => p.Box)
                .WithMany(p => p.OrderedThings)
                .HasForeignKey(p => p.Id);

            modelBuilder.Entity<Return>()
                .HasOne(p => p.OrderedThing)
                .WithOne("Return");
            
            modelBuilder.Entity<Return>()
                .HasOne(p => p.ReturnBox)
                .WithMany(p => p.Returns)
                .HasForeignKey(p => p.ReturnBoxId); ;
        }
    }
}