using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class Cell
    {
        [Key]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("order_id")]
        [JsonPropertyName("order_id")]
        public int OrderId { get; set; }
        
        [Column("is_full")]
        [JsonPropertyName("is_full")]
        public bool IsFull { get; set; }
        
        [JsonIgnore]
        public Order Order { get; set; }
        
        public Cell(int id, int orderId, bool isFull)
        {
            Id = id;
            OrderId = orderId;
            IsFull = isFull;
        }
        
        public Cell(){}
        
        public Cell Clone()
        {
            return new Cell()
            {
                Id = Id,
                OrderId = OrderId,
                IsFull = IsFull,
            };
        }
    }
}