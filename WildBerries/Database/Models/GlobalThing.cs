using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class GlobalThing
    {
        [Key]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        [Column("description")]
        [JsonPropertyName("description")]
        public string Description { get; set; }
        
        [JsonIgnore]
        public List<OrderedThing> OrderedThings { get; set; }
        
        public GlobalThing(int id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }
        
        public GlobalThing() {}
        
        public GlobalThing Clone()
        {
            return new GlobalThing()
            {
                Id = Id,
                Name = Name,
                Description = Description,
            };
        }
    }
}