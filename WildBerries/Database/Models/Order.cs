using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class Order
    {
        [Key]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("buyer_id")]
        [JsonPropertyName("buyer_id")]
        public int BuyerId { get; set; }
        
        [Column("is_complected")]
        [JsonPropertyName("is_complected")]
        public bool IsComplected { get; set; }
        
        [JsonIgnore]
        public Buyer Buyer { get; set; }
        
        [JsonIgnore]
        public List<OrderedThing> OrderedThings { get; set; }
        
        [JsonIgnore]
        public List<Cell> Cells { get; set; }
        
        public Order(int id, int buyerId, bool isComplited)
        {
            Id = id;
            BuyerId = buyerId;
            IsComplected = isComplited;
        }
        
        public Order() {}
        public Order Clone()
        {
            return new Order()
            {
                Id = Id,
                BuyerId = BuyerId,
                IsComplected = IsComplected,
            };
        }
    }
}