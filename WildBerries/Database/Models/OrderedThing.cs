using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class OrderedThing
    {
        [Key]
        [ForeignKey("WrongThing")]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("global_thing_id")]
        [JsonPropertyName("global_thing_id")]
        public int GlobalThingId { get; set; }
        
        [Column("order_id")]
        [JsonPropertyName("order_id")]
        public int OrderId { get; set; }
        
        [Column("box_id")]
        [JsonPropertyName("box_id")]
        public int BoxId { get; set; }
        
        [Column("ordered_thing_status")]
        [JsonPropertyName("ordered_thing_status")]
        public OrderedThingStatus OrderedThingStatus { get; set; }
        
        [Column("ordered_thing_price")]
        [JsonPropertyName("ordered_thing_price")]
        public double OrderedThingPrice { get; set; }
        
        [Column("is_confirmed")]
        [JsonPropertyName("is_confirmed")]
        public bool IsConfirmed { get; set; }
        
        [Column("is_changed")]
        [JsonPropertyName("is_changed")]
        public bool IsChanged { get; set; }
        
        [Column("is_get_before_sync")]
        [JsonPropertyName("is_get_before_sync")]
        public bool IsGetBeforeSync { get; set; }
        
        [JsonIgnore]
        public Order Order { get; set; }
        
        [JsonIgnore]
        public Return Return { get; set; }
        
        [JsonIgnore]
        public GlobalThing GlobalThing { get; set; }
        
        [JsonIgnore]
        public Box Box { get; set; }
        
        public OrderedThing(int id, int globalThingId, int boxId, OrderedThingStatus orderedThingStatus,
            double orderedThingPrice, bool isConfirmed)
        {
            Id = id;
            GlobalThingId = globalThingId;
            BoxId = boxId;
            OrderedThingStatus = orderedThingStatus;
            OrderedThingPrice = orderedThingPrice;
            IsConfirmed = isConfirmed;
        }  
        
        public OrderedThing(){}
        public OrderedThing Clone()
        {
            return new OrderedThing()
            {
                Id = Id,
                GlobalThingId = GlobalThingId,
                BoxId = BoxId,
                OrderedThingStatus = OrderedThingStatus,
                OrderedThingPrice = OrderedThingPrice,
                IsConfirmed = IsConfirmed,
            };
        }
    }

    public enum OrderedThingStatus
    {
        Formed,
        Send,
        Awaiting,
        Accepted,
        Returned
    }
}