using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class Return
    {
        [Key]
        [Column("ordered_thing_id")]
        [JsonPropertyName("ordered_thing_id")]
        public int OrderedThingId { get; set; }
        
        [Column("return_box_id")]
        [JsonPropertyName("return_box_id")]
        public int ReturnBoxId { get; set; }
        
        [Column("return_type")]
        [JsonPropertyName("return_type")]
        public ReturnType ReturnType { get; set; }
        
        [Column("is_return_approved")]
        [JsonPropertyName("is_return_approved")]
        public bool IsReturnApproved { get; set; }
        
        [Column("is_changed")]
        [JsonPropertyName("is_changed")]
        public bool IsChanged { get; set; }
        
        [JsonIgnore]
        public OrderedThing OrderedThing { get; set; }
        
        [JsonIgnore]
        public ReturnBox ReturnBox { get; set; }
        
        public Return(int orderedThingId, int returnBoxId, ReturnType returnType, bool isReturnApproved)
        {
            OrderedThingId = orderedThingId;
            ReturnBoxId = returnBoxId;
            ReturnType = returnType;
            IsReturnApproved = isReturnApproved;
        }
        
        public Return() {}
        
        public Return Clone()
        {
            return new Return()
            {
                OrderedThingId = OrderedThingId,
                ReturnBoxId = ReturnBoxId,
                ReturnType = ReturnType,
                IsReturnApproved = IsReturnApproved,
            };
        }
    }

    public enum ReturnType
    {
        Refund,
        Reclamation,
        Wrong
    }
}