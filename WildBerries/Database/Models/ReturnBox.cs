using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class ReturnBox
    {
        [Key]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("status")]
        [JsonPropertyName("status")]
        public ReturnBoxStatus Status { get; set; }
        
        [Column("is_changed")]
        [JsonPropertyName("is_changed")]
        public bool IsChanged { get; set; }
        
        [JsonIgnore]
        public List<Return> Returns { get; set; }
        
        public ReturnBox(int id, ReturnBoxStatus returnBoxStatus)
        {
            Id = id;
            Status = returnBoxStatus;
        }
        
        public ReturnBox() {}
        
        public ReturnBox Clone()
        {
            return new ReturnBox()
            {
                Id = Id,
                Status = Status,
            };
        }
    }

    public enum ReturnBoxStatus
    {
        Sended,
        Combine
    }
}