using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Database.Models
{
    public class WrongThing
    {
        [Key]
        [Column("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [Column("error_status")]
        [JsonPropertyName("error_status")]
        public ErrorStatus Status { get; set; }
        
        [Column("is_changed")]
        [JsonPropertyName("is_changed")]
        public bool IsChanged { get; set; }
        
        [JsonIgnore]
        public OrderedThing OrderedThing { get; set; }
        
        public WrongThing(int id, ErrorStatus status)
        {
            Id = id;
            Status = status;
        }
        
        public WrongThing() {}
        
        public WrongThing Clone()
        {
            return new WrongThing()
            {
                Id = Id,
                Status = Status,
            };
        }
    }

    public enum ErrorStatus
    {
        More,
        Less,
        Unrecognized
    }
}